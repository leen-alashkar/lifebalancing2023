<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\Http\Resources\CompanyResource;
use Carbon\Carbon;
use App\Http\Controllers\API\BaseController as BaseController;

class CompanyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
      //  $now = Carbon::now();
        $companies = Company::paginate(10);

        return $this->sendResponse(CompanyResource::collection($companies), __('messages.companySucessRetrieved'));
    }

}
